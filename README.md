# DownEm*

This is a simple python script that'll download all files that match a particular regular expression linked to from a page.

The itch it scratches for me is when I want to download the notes for one of my uni courses and they're all PDFs linked to from a single page.

Now I can just run `downem http://page.tld/stuff .*\.pdf` and it'll grab all of them.

# TODO

Stop it from downloading duplicates of files
