#!/usr/bin/env python2

import re
from subprocess import call
from sys import argv
import urllib2 as ul
from urlparse import urljoin

from BeautifulSoup import BeautifulSoup, SoupStrainer

url = argv[1]
regex = argv[2]

matcher = re.compile(regex)
html = ul.urlopen(url).read()
soup = BeautifulSoup(html, convertEntities="html", parseOnlyThese=SoupStrainer('a'))
links = []
for link in soup:
    if link.has_key('href'):
        href = urljoin(url, link['href'])
        if matcher.match(href):
            call("wget -nv %s" % href, shell=True)
            links.append(href)
